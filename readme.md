[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

# Create regressors according to design timing

For the MRI analyses, we need to track when what kind of event happened relative to the BOLD recordings. Here, we create regressor files that include this information.

**a_extractMRTiming**: creates regressors
**b_convertToCSVFilesForBIDS**: converts .mat regressors to .csv files to share alongside BIDS data

*Note that both steps would ideally be associated with creating the BIDS data. This dataset was not originally based on the BIDS standard. Therefore this step was done later on in the pipeline. If BIDS conversion is the first step in a data workflow, these steps should be taken in the beginning and in accordance with the BIDS specification.*

To identify when what event happened, we need to consider the acquisition and protocol structure of this experiment. Events are logged in the 'ExperimentProtocol' structure of the behavioral .mat file that was output from Psychtoolbox. The structure was as follows:

* For design details, see https://git.mpib-berlin.mpg.de/LNDG/multi-attribute-task/-/blob/master/StateSwitch/StateSwitch_experiment_171023.m
* Participants had to indicate by button press that they are ready
* The script waits for 17 TRs (at TR of .625s) to allow for BOLD to reach steady-state
* Following this waiting time, the script defines t0
* The script encodes 'RunInitiation' and t0 as timingInfo.t0_TROnset in 'ExperimentProtocol'
* In the script below we have to account for the 17 TRs that happened before 'RunInitiation'
* During preprocessing, onset volumes are removed, in this case we also account for this. If we were to provide csv files corresponding to raw data this should not be the case.

## Specific notes

    The first version (v1) did not take into account that the paradigm only started at the 17th trigger/TR/volume. We thus need to remove the first 16 volumes from the calculation (4 following preprocessing).

    Version 3 added additional regressors of interest: 

    * probe onset (button press may make little sense here, as it is so close to the probe onset, basically merging them into the same regressor)
    * stimulus attribute regressors
    * probe accuracy
    * parametric probe RT

    Note that these regressors could always be modulated outside (i.e. here we only have a stimulus onset regressor, but we can easily calculate a stimulus ON regressor from extending the stim onset regressor until probe onset)

    This version also switched to the most recent merged behavioural data structure (info should be identical though).
