% This script serves to create event timeseries for the task MR data.

% 180220 | written by JQK
% 181112 | added fixation regressor

% v3

% Required information: block onsets and block duration (in TRs respective to initial TR in preprocessed data)
% regressor event time series with respect to first volume in preprocessed data

clear all; clc;

pn.dataIn = '/Volumes/LNDG/Projects/StateSwitch/dynamic/raw/C_study/mri/behavior_eye/*';

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

%% subject IDs

% N = 44; + N = 53 (excluding pilots);
ID = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261';...
    '1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';...
    '1215';'1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';...
    '1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';...
    '1268';'1270';'1276';'1281'};

%% general timing information

timingInfo.numofVolumes_raw = 1066;
timingInfo.numofVolumes_preproc = 1054; % 12 volumes deleted prior to FEAT
timingInfo.TR = .645; % in seconds
timingInfo.t0_TROnset = 17; % the program waited for 17 TRs before sending the t0 trigger.

%% find out for which subjects, multiple files exist, indicating crashes

for indID = 1:numel(ID)
    files = dir([pn.dataIn, '/', ID{indID}, '_StateSwitchMR_dynamic*']);
    numFiles(indID) = numel(files);
end
ID(find(numFiles==2))

% Note: for the following subjects multiple files exist:
%   1135: run 1, 234
%   1173: run 1, 234
%   1228: run 1, 234
%   1250: run 1, 234

%   2104: run 12, 34
%   2121: run 123, 4
%   2132: run 12, 34
%   2216: run 12, 34
%   2258: run 1, 234

%% merge individual data

amountRunsByRun = 1;
amountBlocksByRun = 8;
amountTrialsByRun = 64;
amountFixsByRun = 64;
amountStimsByRun = 64;

problematicIDs.IDs = {'1135'; '1173'; '1228'; '1250'; '2104'; '2121'; '2132'; '2216'; '2258'};
problematicIDs.runsFile1 = [1, 1, 1, 1, 2, 3, 2, 2, 1];

timingInfo.events.RunOnsets     = cell(1,numel(ID));
timingInfo.events.BlockOnsets	= cell(1,numel(ID));
timingInfo.events.TrialOnsets	= cell(1,numel(ID));
timingInfo.events.FixOnsets     = cell(1,numel(ID));
timingInfo.events.StimOnsets    = cell(1,numel(ID));
timingInfo.events.ProbeOnsets   = cell(1,numel(ID));

for indID = 1:numel(ID)
    files = dir([pn.dataIn, '/', ID{indID}, '_StateSwitchMR_dynamic*']);
    for indFile = 1:numel(files) % put together runs if there are multiple files (e.g. PTB crash between runs)
        load([files(indFile).folder, '/', files(indFile).name], 'ExperimentProtocol');
        
        idx = []; tmp = [];
        
        % extract run onsets
        idx.RunOnsets = find(strcmp(ExperimentProtocol(:,1), 'RunInitiation'));
        tmp.RunOnsets = cell2mat(ExperimentProtocol(idx.RunOnsets,2));
        
        % extract block onsets
        idx.BlockOnsets = find(strcmp(ExperimentProtocol(:,1), 'BlockInitiation'));
        tmp.BlockOnsets = cell2mat(ExperimentProtocol(idx.BlockOnsets,2));
        
        % extract trial onsets (at cue onset)
        idx.TrialOnsets = find(strcmp(ExperimentProtocol(:,1), 'FixCueOnset'));
        tmp.TrialOnsets = cell2mat(ExperimentProtocol(idx.TrialOnsets,2));
        
        % extract fix onsets
        idx.FixOnsets = find(strcmp(ExperimentProtocol(:,1), 'PostCueFixOnset'));
        tmp.FixOnsets = cell2mat(ExperimentProtocol(idx.FixOnsets,2));
        
        % extract stimulus onsets 
        idx.StimOnsets = find(strcmp(ExperimentProtocol(:,1), 'StimOnset'));
        tmp.StimOnsets = cell2mat(ExperimentProtocol(idx.StimOnsets,2));
        
        % extract probe onset
        idx.ProbeOnsets = find(strcmp(ExperimentProtocol(:,1), 'RespOnset'));
        tmp.ProbeOnsets = cell2mat(ExperimentProtocol(idx.ProbeOnsets,2));
        
        % adapt temporary information for deviant data
        if max(strcmp(ID{indID}, problematicIDs.IDs)==1) & indFile == 1
            runsAvailable   = problematicIDs.runsFile1(strcmp(ID{indID}, problematicIDs.IDs));
            tmp.RunOnsets   = tmp.RunOnsets(1:runsAvailable*amountRunsByRun);
            tmp.BlockOnsets = tmp.BlockOnsets(1:runsAvailable*amountBlocksByRun);
            tmp.TrialOnsets = tmp.TrialOnsets(1:runsAvailable*amountTrialsByRun);
            tmp.FixOnsets   = tmp.FixOnsets(1:runsAvailable*amountTrialsByRun);
            tmp.StimOnsets  = tmp.StimOnsets(1:runsAvailable*amountStimsByRun);
            tmp.ProbeOnsets = tmp.ProbeOnsets(1:runsAvailable*amountTrialsByRun);
        end
        
        timingInfo.events.RunOnsets{1,indID}     = [timingInfo.events.RunOnsets{1,indID}; tmp.RunOnsets];
        timingInfo.events.BlockOnsets{1,indID}   = [timingInfo.events.BlockOnsets{1,indID}; tmp.BlockOnsets];
        timingInfo.events.TrialOnsets{1,indID}   = [timingInfo.events.TrialOnsets{1,indID}; tmp.TrialOnsets];
        timingInfo.events.FixOnsets{1,indID}     = [timingInfo.events.FixOnsets{1,indID}; tmp.FixOnsets];
        timingInfo.events.StimOnsets{1,indID}    = [timingInfo.events.StimOnsets{1,indID}; tmp.StimOnsets];
        timingInfo.events.ProbeOnsets{1,indID}   = [timingInfo.events.ProbeOnsets{1,indID}; tmp.ProbeOnsets];
        
        clear ExperimentProtocol;
    end
end

timingInfo.events.RunOnsets{25}(4) = []; % delete double entry for YAs

timingInfo.eventsMat.RunOnsets     = cat(2,timingInfo.events.RunOnsets{:});
timingInfo.eventsMat.BlockOnsets   = cat(2,timingInfo.events.BlockOnsets{:});
timingInfo.eventsMat.TrialOnsets   = cat(2,timingInfo.events.TrialOnsets{:});
timingInfo.eventsMat.FixOnsets     = cat(2,timingInfo.events.FixOnsets{:});
timingInfo.eventsMat.StimOnsets    = cat(2,timingInfo.events.StimOnsets{:});
timingInfo.eventsMat.ProbeOnsets   = cat(2,timingInfo.events.ProbeOnsets{:});

%% for each subject, create run-wise assignment

RegressorMatrix = [];
for indID = 1:numel(ID)
    for indRun = 1:4

        curRuns     = [(indRun-1)*amountRunsByRun+1:indRun*amountRunsByRun];
        curBlocks   = [(indRun-1)*amountBlocksByRun+1:indRun*amountBlocksByRun];
        curTrials   = [(indRun-1)*amountTrialsByRun+1:indRun*amountTrialsByRun];
        curFixs     = [(indRun-1)*amountFixsByRun+1:indRun*amountFixsByRun];
        curStims    = [(indRun-1)*amountStimsByRun+1:indRun*amountStimsByRun];

        OnsetTime = timingInfo.eventsMat.RunOnsets(curRuns,indID);
        
        % Calculate time of each TR acquisition (timing of original acquisition)
        
        % the experiment was started on the 17th TR
        TRsToDiscard = timingInfo.t0_TROnset-1;
        
        % Note that we generally only collect a single onset trigger in the
        % beginning. ANY jitter in the actual acquisition of volumes would
        % result in timing inaccuracies.
                
        TRtiming = [0, cumsum(repmat(timingInfo.TR, 1,timingInfo.numofVolumes_raw-1))];
        TRtiming_cur = [sort(OnsetTime-TRtiming(2:TRsToDiscard+1),'ascend'), OnsetTime+TRtiming(1:timingInfo.numofVolumes_raw-TRsToDiscard)];
        % find(TRtiming_cur==OnsetTime) onset should be at 17th entry
        
        % blocks
        A = timingInfo.eventsMat.BlockOnsets(curBlocks,indID);
        B = TRtiming_cur;
        TMP = bsxfun(@(x,y) abs(x-y), A(:), reshape(B,1,[]));
        [~, idx.blocks] = min(TMP,[],2); clear A B TMP;
        
        % trials
        A = timingInfo.eventsMat.TrialOnsets(curTrials,indID);
        B = TRtiming_cur;
        TMP = bsxfun(@(x,y) abs(x-y), A(:), reshape(B,1,[]));
        [~, idx.trials] = min(TMP,[],2); clear A B TMP;
        
        % fixs
        A = timingInfo.eventsMat.FixOnsets(curTrials,indID);
        B = TRtiming_cur;
        TMP = bsxfun(@(x,y) abs(x-y), A(:), reshape(B,1,[]));
        [~, idx.fixs] = min(TMP,[],2); clear A B TMP;
        
        % stims
        A = timingInfo.eventsMat.StimOnsets(curStims,indID);
        B = TRtiming_cur;
        TMP = bsxfun(@(x,y) abs(x-y), A(:), reshape(B,1,[]));
        [~, idx.stims] = min(TMP,[],2); clear A B TMP;
        
        % probes
        A = timingInfo.eventsMat.ProbeOnsets(curStims,indID);
        B = TRtiming_cur;
        TMP = bsxfun(@(x,y) abs(x-y), A(:), reshape(B,1,[]));
        [~, idx.probes] = min(TMP,[],2); clear A B TMP;
        
        RegressorMatrix{indID,indRun}(:,1) = zeros(timingInfo.numofVolumes_raw,1);
        RegressorMatrix{indID,indRun}(idx.blocks,1) = 1;
        RegressorMatrix{indID,indRun}(:,2) = zeros(timingInfo.numofVolumes_raw,1);
        RegressorMatrix{indID,indRun}(idx.trials,2) = 1;
        RegressorMatrix{indID,indRun}(:,3) = zeros(timingInfo.numofVolumes_raw,1);
        RegressorMatrix{indID,indRun}(idx.stims,3) = 1;
        RegressorMatrix{indID,indRun}(:,11) = zeros(timingInfo.numofVolumes_raw,1);
        RegressorMatrix{indID,indRun}(idx.probes,11) = 1;
        RegressorMatrix{indID,indRun}(:,14) = zeros(timingInfo.numofVolumes_raw,1);
        RegressorMatrix{indID,indRun}(idx.fixs,14) = 1;
        
    clear cur*;
    end
end

RegressorInfo{1,1} = 'block onset dummy';
RegressorInfo{1,2} = 'trial onset dummy';
RegressorInfo{1,3} = 'stim onset dummy';
RegressorInfo{1,11} = 'probe onset dummy';
RegressorInfo{1,14} = 'fix onset dummy';

figure;
for indID = 1%:numel(ID)
    hold on;
    plot(RegressorMatrix{indID,1}(:,2), 'r')
    plot(RegressorMatrix{indID,1}(:,3), 'k')
    plot(RegressorMatrix{indID,1}(:,11), 'g')
end

%% cut off the data that has been excluded during preprocessing

% No data should be lost as we removed less data than we waited for.

removedVolumes = timingInfo.numofVolumes_raw-timingInfo.numofVolumes_preproc;

RegressorMatrixPreproc = RegressorMatrix;
for indID = 1:numel(ID)
    for indRun = 1:4
        for indCol = 1:size(RegressorMatrix{indID,indRun},2)
            if find(RegressorMatrix{indID,indRun}(1:removedVolumes,indCol))
                RegressorMatrixPreproc{indID,indRun}(removedVolumes+1,indCol) = 1;
            end
        end
        RegressorMatrixPreproc{indID,indRun}(1:removedVolumes,:) = [];
    end
end

%% for regressors, extract behavioral data

% Behavioral data to include:
% - stimulus sequence
% - dimensionality (block, stim, trial)
% - stimulus focus attributes (should only be on until stimulus offset)

% load behavioral data (already extracted)
behavData = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat');

% go through subject and runs and create relevant regressors
for indID = 1:numel(ID)
    for indRun = 1:4

        % make sure that the behavioral IDs match the IDs above!!!
        behavIdx = find(strcmp(behavData.IDs_all, ID{indID})); % get index of current subject in behavioral matrix
        SubjBehavData.Atts          = behavData.MergedDataMRI.Atts(:,behavIdx);
        SubjBehavData.StateOrders   = behavData.MergedDataMRI.StateOrders(:,behavIdx);
        SubjBehavData.RTs           = behavData.MergedDataMRI.RTs(:,behavIdx);
        SubjBehavData.Accs          = behavData.MergedDataMRI.Accs(:,behavIdx);
        SubjBehavData.CuedAttributes = reshape(cat(1,behavData.MergedDataMRI.expInfo{behavIdx}.AttCuesRun{:})',256,1);

        % create matrix with crucial condition info
        CondData = NaN(256,7);
        for indTrial = 1:256
           tmp_cuedTrial = SubjBehavData.CuedAttributes(indTrial);
           if ismember(1,tmp_cuedTrial{:})
               CondData(indTrial,1) = 1;
           end
           if ismember(2,tmp_cuedTrial{:})
               CondData(indTrial,2) = 1;
           end
           if ismember(3,tmp_cuedTrial{:})
               CondData(indTrial,3) = 1;
           end
           if ismember(4,tmp_cuedTrial{:})
               CondData(indTrial,4) = 1;
           end
        end
        CondData(:,5) = repmat(1:8,1,32)'; % serial position
        CondData(:,6) = SubjBehavData.Atts;
        CondData(:,7) = 1:256;
        CondData(:,8) = SubjBehavData.StateOrders;
        CondData(:,9) = SubjBehavData.RTs;
        CondData(:,10) = SubjBehavData.Accs;
        % select trials from current run
        CurTrials = CondData(((indRun-1)*64)+1:indRun*64,:);
        CurTrials(isnan(CurTrials))=0;
        CurBlocks = CurTrials(:,8);
        CurBlocks(diff(CurBlocks)==0) = []; % remove adjacent repetitions
        
        %% enter information into the regressors
        
        % col1: block onsets [dims]
        % col2: trial onsets [serial pos, atts]
        % col3: stim onsets [serial pos, atts]
        
        indCol = 4;
        idx_block = find(RegressorMatrixPreproc{indID,indRun}(:,1));
        RegressorMatrixPreproc{indID,indRun}(:,indCol) = zeros(1,timingInfo.numofVolumes_preproc);
        RegressorMatrixPreproc{indID,indRun}(idx_block,indCol) = CurBlocks;
        RegressorInfo{1,indCol} = 'block: dimensionality';
        
        indCol = 5;
        idx_trial = find(RegressorMatrixPreproc{indID,indRun}(:,2));
        RegressorMatrixPreproc{indID,indRun}(:,indCol) = zeros(1,timingInfo.numofVolumes_preproc);
        RegressorMatrixPreproc{indID,indRun}(idx_trial,indCol) = CurTrials(:,5);
        RegressorInfo{1,indCol} = 'trial: serial position';
        
        indCol = 6;
        idx_stim = find(RegressorMatrixPreproc{indID,indRun}(:,3));
        RegressorMatrixPreproc{indID,indRun}(:,indCol) = zeros(1,timingInfo.numofVolumes_preproc);
        RegressorMatrixPreproc{indID,indRun}(idx_stim,indCol) = CurTrials(:,5);
        RegressorInfo{1,indCol} = 'stim: serial position';
        
        indCol = 7;
        idx_stim = find(RegressorMatrixPreproc{indID,indRun}(:,3));
        RegressorMatrixPreproc{indID,indRun}(:,indCol) = zeros(1,timingInfo.numofVolumes_preproc);
        RegressorMatrixPreproc{indID,indRun}(idx_stim,indCol) = CurTrials(:,1);
        RegressorInfo{1,indCol} = 'stim: att1';
        
        indCol = 8;
        idx_stim = find(RegressorMatrixPreproc{indID,indRun}(:,3));
        RegressorMatrixPreproc{indID,indRun}(:,indCol) = zeros(1,timingInfo.numofVolumes_preproc);
        RegressorMatrixPreproc{indID,indRun}(idx_stim,indCol) = CurTrials(:,2);
        RegressorInfo{1,indCol} = 'stim: att2';
        
        indCol = 9;
        idx_stim = find(RegressorMatrixPreproc{indID,indRun}(:,3));
        RegressorMatrixPreproc{indID,indRun}(:,indCol) = zeros(1,timingInfo.numofVolumes_preproc);
        RegressorMatrixPreproc{indID,indRun}(idx_stim,indCol) = CurTrials(:,3);
        RegressorInfo{1,indCol} = 'stim: att3';
        
        indCol = 10;
        idx_stim = find(RegressorMatrixPreproc{indID,indRun}(:,3));
        RegressorMatrixPreproc{indID,indRun}(:,indCol) = zeros(1,timingInfo.numofVolumes_preproc);
        RegressorMatrixPreproc{indID,indRun}(idx_stim,indCol) = CurTrials(:,4);
        RegressorInfo{1,indCol} = 'stim: att4';
        
        indCol = 12;
        idx_probe = find(RegressorMatrixPreproc{indID,indRun}(:,11));
        RegressorMatrixPreproc{indID,indRun}(:,indCol) = zeros(1,timingInfo.numofVolumes_preproc);
        RegressorMatrixPreproc{indID,indRun}(idx_probe,indCol) = CurTrials(:,10);
        RegressorInfo{1,indCol} = 'probe: accuracy';
        
        indCol = 13;
        idx_probe = find(RegressorMatrixPreproc{indID,indRun}(:,11));
        RegressorMatrixPreproc{indID,indRun}(:,indCol) = zeros(1,timingInfo.numofVolumes_preproc);
        RegressorMatrixPreproc{indID,indRun}(idx_probe,indCol) = CurTrials(:,9);
        RegressorInfo{1,indCol} = 'probe: RT';

        %% save regressor
        
        Regressors = RegressorMatrixPreproc{indID,indRun};
        save([fullfile(rootpath, 'data', 'regressors'), ...
            ID{indID}, '_Run', num2str(indRun),'_regressors.mat'], ...
            'Regressors', 'RegressorInfo');
        clear Regressors;
        
    end
end